#include "main.h"

/* MODBUS */
volatile uint8_t instate = 0;
volatile uint8_t outstate = 0;
volatile uint16_t inputRegisters[64];
volatile uint16_t holdingRegisters[64];

/* Enable inverter operation by toggling the shutdown pin on the eval6491 boards */
volatile bool enable_operation = false;
/* Control variable updated via Modbus for indexing the correct LUT */
// volatile uint16_t bank_voltage;

uint8_t ReadIns(void);
void SetOuts(volatile uint8_t in);
void modbusGet(void);
void translate_modbus(void);

int main(void)
{
	/* Pin Configuration */
	Pin_Init();

	/* Timer Init */
	Timer_Init();

	/* Modbus Init */
	//PORTD &=~ _BV(TRANSCEIVER_ENABLE_PIN); //TRANSCEIVER_ENABLE_PIN

	modbusSetAddress(modbusAddress);
	modbusInit();
	modbusReset();

	for (int i = 0; i < HOLDINGREGISTERLENGTH; ++i)
    {
        holdingRegisters[i] = 42;
    }

	/* Interrupt Enable */
	sei();

	while(1)
	{
		/*if(!enable_operation)
		{
			PORTD&=~_BV(PD7);
		}
		else
		{
			PORTD|=_BV(PD7);
		}*/
		modbusGet();
		translate_modbus();
	}
	return 0;
}

uint8_t ReadIns(void) {
	uint8_t ins=0x00;
	ins|=(PINC&((1<<0)|(1<<1)|(1<<2)|(1<<3)|(1<<4)|(1<<5)));
	ins|=(((PIND&(1<<4))<<2)|((PIND&(1<<3))<<4));
	return ins;
}

void SetOuts(volatile uint8_t in) {
	PORTD|= (((in & (1<<3))<<4) | ((in & (1<<4))<<1) | ((in & (1<<5))<<1));
	PORTB|= (((in & (1<<0))<<2) | ((in & (1<<1))) | ((in & (1<<2))>>2));
	in=~in;
	PORTB&= ~(((in & (1<<0))<<2) | ((in & (1<<1))) | ((in & (1<<2))>>2));
	PORTD&= ~(((in & (1<<3))<<4) | ((in & (1<<4))<<1) | ((in & (1<<5))<<1));

}

void translate_modbus()
{
	if (holdingRegisters[0] & ((1<<3)|(1<<4)|(1<<5)|(1<<6)|(1<<7)))
	{
		//ERROR: not implemented!
	}
	else
	{
		enable_operation	= holdingRegisters[0];
		bank_voltage		= (holdingRegisters[32]-355)/6;
	}
}

void modbusGet()
{
	if (modbusGetBusState() & (1<<ReceiveCompleted))
	{
		switch(rxbuffer[1]) {
			case fcReadCoilStatus: {
				modbusExchangeBits(&outstate,0,8);
			}
			break;

			case fcReadInputStatus: {
				volatile uint8_t inps = ReadIns();
				modbusExchangeBits(&inps,0,8);

			}
			break;

			case fcReadHoldingRegisters: {
				modbusExchangeRegisters(holdingRegisters,0,64);

			}
			break;

			case fcReadInputRegisters: {
				modbusExchangeRegisters(inputRegisters,0,64);
			}
			break;

			case fcForceSingleCoil: {
				modbusExchangeBits(&outstate,0,8);
				SetOuts(outstate);
			}
			break;

			case fcPresetSingleRegister: {
				modbusExchangeRegisters(holdingRegisters,0, HOLDINGREGISTERLENGTH);
			}
			break;

			case fcForceMultipleCoils: {
				modbusExchangeBits(&outstate,0,8);
				SetOuts(outstate);
			}
			break;

			case fcPresetMultipleRegisters: {
				modbusExchangeRegisters(holdingRegisters,0, HOLDINGREGISTERLENGTH);
			}
			break;

			default: {
				modbusSendException(ecIllegalFunction);

			}
			break;
		}
	}
}
