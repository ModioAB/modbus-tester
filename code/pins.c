/*
 * pin.c
 *
 * Created: 7/27/2017 7:08:41 PM
 *  Author: Eric
 */ 
#include "pins.h"

void Pin_Init(void)
{
	DDRB = 0b00101010; //PB1 = PWM, PB3 = PWM, PB5 = LED
	DDRD = 0b11001110; //PD1 = TX, PD2 = DE, PD3 =RE, PD6 = PWM, PD7 = Shutdown inverter if 1
}