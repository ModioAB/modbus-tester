/*
 * globals.h
 *
 * Created: 8/5/2017 3:26:57 PM
 *  Author: Eric
 */ 


#ifndef GLOBALS_H_
#define GLOBALS_H_

#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

extern volatile uint16_t bank_voltage;

#endif /* GLOBALS_H_ */
