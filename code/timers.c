/*
 * timers.c
 *
 * Created: 7/27/2017 7:04:44 PM
 *  Author: Eric
 */ 
#include "timers.h"
#include "LUT.h"
#include "main.h"
//unsigned int feedback = 0;

const unsigned int phase = 209;

void Timer_Init()
{
	/* Timer halt */
	GTCCR = (1<<TSM)|(1<<PSRASY)|(1<<PSRSYNC);
	
	/* Timer Configuration */
	TCCR0A = 0;
	TCCR0B = 0;
	TCCR0A |= _BV(COM0A1)| _BV(WGM00);
	TCCR0B |= _BV(CS00);

	TCCR1A = 0;
	TCCR1B = 0;
	TCCR1A |= _BV(COM1A1) | _BV(WGM10);
	TCCR1B |= _BV(CS10);

	TCCR2A = 0;
	TCCR2B = 0;
	TCCR2A |= _BV(COM2A1) | _BV(WGM20);
	TCCR2B |= _BV(CS20);

	TIMSK0 = 0;
	TIMSK1 = 0;
	TIMSK2 = 0;
	TIMSK0 |= _BV(TOIE0);
	TIMSK1 |= _BV(TOIE1);
	TIMSK2 |= _BV(TOIE2);
	
	TCNT0 = 0; // set timer0 to 0
	TCNT1H = 0; // set timer1 high byte to 0
	TCNT1L = 0; // set timer1 low byte to 0
	TCNT2 = 0; // set timer2 to 0

	OCR0A =pgm_read_byte(&(lookUp[0][0])); //Load compare register with 0
	OCR1A = pgm_read_byte(&(lookUp[0][0]));
	OCR2A = pgm_read_byte(&(lookUp[0][0]));

	GTCCR = 0; // release all timers
}

ISR(TIMER0_OVF_vect) //Timer/Counter Overflow Flag, Timer 0 has reached zero
{
	static unsigned int x = 0;
	OCR0A = pgm_read_byte(&(lookUp[bank_voltage-1][x]));
	x++;
	if(x == 627)
	{
		x = 0;
	}
}

ISR(TIMER1_OVF_vect) //Timer/Counter Overflow Flag, Timer 1 has reached zero
{
	static int y = phase;
	y++;
	OCR1A = pgm_read_byte(&(lookUp[bank_voltage-1][y]));
	if(y == 627)
	{
		y = 0;
	}
}

ISR(TIMER2_OVF_vect) //Timer/Counter Overflow Flag, Timer 2 has reached zero
{
	/* PWM */
	static int z = 2*phase;
	z++;
	OCR2A = pgm_read_byte(&(lookUp[bank_voltage-1][z]));
	if(z == 627)
	{
		z = 0;
	} 
	
	/* MODBUS */
	static int count = 0;
	if(count >= 3)
	{
		modbusTickTimer();
		count = 0;
	}
	else
	{
		count++;
	}
}