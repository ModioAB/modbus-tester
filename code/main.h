#ifndef MAIN_H_
#define MAIN_H_

#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "Modbus_lib/yaMBSiavr.h"
#include "timers.h"
#include "pins.h"
#include "globals.h"

#define modbusAddress 0x09
#define HOLDINGREGISTERLENGTH 64

#endif /* MAIN_H_ */
