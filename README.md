"A simple hardware and software to test modbus devices.  
Talks modbus and responds to the address printed on the back of the board.  
 
Using Arduino nano and modbusino library.

Code is licensed under GPL, see code/LICENSE
